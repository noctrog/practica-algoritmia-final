#!/bin/sh

for problem in ./tests/*.p
do
	echo -n $problem
	output=$(./met_bb -f $problem)
	result=$(echo "$output" | sed 1q | awk '{print $1}')
	expected=$(cat $problem.sol_bb | sed 1q | awk '{print $2}')
	if [[ $result == $expected ]]; then
		echo ": Ok"
	else 
		echo ": FAIL"
	fi
done
