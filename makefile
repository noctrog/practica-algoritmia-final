#Ramón Calvo González 54205655Z	

CXX=g++
CFLAGS=-std=c++11 -Wall -pedantic -O3

.PHONY=clean test

met_bb: met_bb.cc
	$(CXX) $(CFLAGS) $^ -o $@

test: met_bb
	@sh ./test.sh

clean:
	@rm -f met_bb
