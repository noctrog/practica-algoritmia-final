// Ramon Calvo Gonzalez 54205655Z

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <numeric>
#include <chrono>
#include <queue>
#include <unordered_set>

using matrix = std::vector<std::vector<float>>;
using gateways = std::vector<bool>;
//	gateways	cost	current_m	current_pos
// using node = std::tuple<gateways, float, size_t, size_t>;
struct node {
	gateways gws;
	float cost;
	size_t current_m, current_pos;
	bool is_leaf;
};

auto compare_func = [](const node& a, const node& b) {return a.cost < b.cost;};
using priority_queue = std::priority_queue<node, std::vector<node>, decltype(compare_func)>;

struct NodeCount {
	size_t nodos_explorados;
	size_t nodos_anadidos;
	size_t nodos_no_prometedores, nodos_no_factibles;
	size_t nodos_completados;
	size_t nodos_prometedores_descartados;
	size_t nodos_expandidos;
	size_t veces_solucion_actualizada;
	size_t veces_solucion_actulizada_cota_pesimista;
};

class Problem {
	size_t n, m;
	std::vector<float> c;
	size_t rows, cols;
	std::vector<std::vector<float>> M;

public:
	Problem();
	Problem(std::string& file_path);
	~Problem();

	size_t get_n(void) const;
	size_t get_m(void) const;
	const matrix& get_M(void) const;
	size_t get_cols(void) const;
	size_t get_rows(void) const;

	float cost(size_t a, size_t b) const;
	std::vector<size_t> sort_by_cost(void) const;
	float calc_cost(const std::vector<size_t>& indices) const;
	float calc_cost(const std::vector<bool>& indices) const;
	float calc_cost(const std::vector<bool>& indices, const std::vector<size_t>& order) const;
	float calc_cost(const std::unordered_set<size_t>& gateways,
			const std::unordered_set<size_t>& nodes,
			const std::vector<size_t>& order) const;
	std::vector<size_t> routing(const std::vector<size_t>& gateways) const;

	void load(std::string& file_path);
};

Problem::Problem()
{

}

Problem::Problem(std::string& file_path)
{
	load(file_path);
}

Problem::~Problem()
{

}

size_t Problem::get_n() const
{
	return n;
}

size_t Problem::get_m() const
{
	return m;
}

const matrix& Problem::get_M(void) const
{
	return M;
}

size_t Problem::get_cols() const
{
	return cols;
}

size_t Problem::get_rows() const
{
	return rows;
}

void Problem::load(std::string& file_path)
{
	std::ifstream file;
	file.open(file_path, std::ios_base::in);

	if (!file.is_open()) {
		std::runtime_error("Error al abrir el archivo!");
	}

	std::string line;
	std::getline(file, line);
	std::stringstream iss(line);

	// Leer n y m 
	iss >> n >> m;
	M = std::vector<std::vector<float>>(n);	
	// Leer c
	float aux;
	std::getline(file, line);
	iss = std::stringstream(line);
	while (iss >> aux) {
		c.push_back(aux);
	}

	// Leer la matriz
	for (size_t i = 0; i < n; ++i){
		// Leer una file
		std::getline(file, line);
		iss = std::stringstream(line);
		while (iss >> aux) {
			M.at(i).push_back(aux);
		}
	}

	rows = M.size();
	cols = M[0].size();

	if (cols != rows) {
		std::runtime_error("ERROR: numero de filas y columnas no coinciden");
	}
}

float Problem::cost(size_t a, size_t b) const
{
	float cost = 0.0f;

	if (a < rows && b < rows)
		cost = M[a][b];

	return cost;
}

std::vector<size_t> Problem::sort_by_cost() const
{
	std::vector<size_t> indices(rows);
	std::iota(indices.begin(), indices.end(), 0);

	std::sort(indices.begin(), indices.end(),
			[this](size_t i, size_t j){return this->c.at(i) > this->c.at(j);});

	return indices;
}

float Problem::calc_cost(const std::vector<size_t> &indices) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (std::find(indices.begin(), indices.end(), i) == indices.end()) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (auto gw : indices) {
				float aux_cost = this->cost(i, gw) * c.at(i);
				if (aux_cost < instance_cost)
					instance_cost = aux_cost;
			}

			cost += instance_cost;
		}
	}

	return cost;
}

float Problem::calc_cost(const std::vector<bool>& indices) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (!indices[i]) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (size_t j = 0; j < rows; ++j) {
				if (indices[j]) {
					float aux_cost = this->cost(i, j) * c.at(i);
					if (aux_cost < instance_cost) {
						instance_cost = aux_cost;
					}
				}
			}

			cost += instance_cost;
		}
	}

	return cost;
}

float Problem::calc_cost(const std::vector<bool>& indices, const std::vector<size_t>& order) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (!indices[order[i]]) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (size_t j = 0; j < rows; ++j) {
				if (indices[order[j]]) {
					float aux_cost = this->cost(order[i], order[j]) * c.at(order[i]);
					if (aux_cost < instance_cost) {
						instance_cost = aux_cost;
					}
				}
			}

			cost += instance_cost;
		}
	}

	return cost;
}

float Problem::calc_cost(const std::unordered_set<size_t>& gateways,
						 const std::unordered_set<size_t>& nodes,
						 const std::vector<size_t>& order) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (auto it = nodes.begin(); it != nodes.end(); ++it) {
		// Probar con cada gateway
		float instance_cost = std::numeric_limits<float>::max();
		for (auto it_gw = gateways.begin(); it_gw != gateways.end(); ++it_gw) {
			float aux_cost = this->cost(*it, *it_gw) * c.at(*it);
			if (aux_cost < instance_cost)
				instance_cost = aux_cost;
		}

		cost += instance_cost;
	}

	return cost;
}

std::vector<size_t> Problem::routing(const std::vector<size_t>& gateways) const
{
	std::vector<size_t> routing(n);

	// Por cada nodo
	for (size_t id = 0; id < n; ++id) {
		// Si el nodo es un gateway, la salida sera el mismo
		if (std::find(gateways.begin(), gateways.end(), id) != gateways.end()) {
			routing[id] = id;
		} else {
			// Mirar que gateway tiene menos peso
			float cost = std::numeric_limits<float>::max();
			size_t gw = 0;
			for (auto aux_gw : gateways) {
				float aux_cost = this->cost(id, aux_gw);
				if (aux_cost < cost) {
					cost = aux_cost;
					gw = aux_gw;
				}
			}

			routing[id] = gw;
		}
	}

	return routing;
}

float greedy_criteria_1(const Problem& problem)
{
	// Obtener los indices ordenados por capacidad de almacenamiento
	std::vector<size_t> sorted_indices = problem.sort_by_cost();

	// Seleccionar dichos indices
	std::vector<size_t> indices(sorted_indices.begin(), sorted_indices.begin() + problem.get_m());

	// Obtiene el coste de esos gateways y lo devuelve
	return problem.calc_cost(indices);
}

float greedy_criteria_1(const Problem& problem, std::vector<size_t>& out_gateways)
{
	// Obtener los indices ordenados por capacidad de almacenamiento
	std::vector<size_t> sorted_indices = problem.sort_by_cost();

	// Seleccionar dichos indices
	std::vector<size_t> indices(sorted_indices.begin(), sorted_indices.begin() + problem.get_m());

	// Guarda los gateways escogidos
	out_gateways = indices;

	// Obtiene el coste de esos gateways y lo devuelve
	return problem.calc_cost(indices);
}

float greedy_criteria_2(const Problem& problem, std::vector<size_t>& out_gateways)
{
	// Inicializar indices
	std::vector<size_t> gateways(problem.get_n());
	std::iota(gateways.begin(), gateways.end(), 0);


	for (size_t iteration = problem.get_n(); iteration > problem.get_m(); --iteration){
		float instance_cost = std::numeric_limits<float>::max();
		size_t gw_a_quitar = 0;

		// Quitar el gateway con el que se reduce mas el peso
		for (auto gw : gateways) {
			// Quitar el gateway actual
			std::vector<size_t> instance_gateways(gateways.begin(), gateways.end());
			instance_gateways.erase(std::find(instance_gateways.begin(), instance_gateways.end(), gw));

			// Calcula el coste y compara
			float aux_cost = problem.calc_cost(instance_gateways);
			if (aux_cost < instance_cost) {
				instance_cost = aux_cost;
				gw_a_quitar = gw;
			}
		}
		gateways.erase(std::find(gateways.begin(), gateways.end(), gw_a_quitar));
	}

	// Guardar los gateways escogidos
	out_gateways = gateways;

	return problem.calc_cost(gateways);
}

bool is_leaf(const Problem& p, const node& n)
{
	if (n.current_m - (p.get_n() - n.current_pos) == p.get_m()) {
		return true;
	} else {
		return false;
	}
}

std::vector<node> expand(const Problem& p, const node& n)
{
	// Si no se puede expandir mas, devuelve un vector vacio
	if (n.current_pos >= p.get_n())
		return std::vector<node>({});

	// Inicializa el vector con dos nodos
	std::vector<node> new_nodes(2, n);

	// Referencias al hijo derecho e izquierdo
	node& left = new_nodes[0];
	node& right = new_nodes[1];

	// El hijo de la izquierda es el que cambia el nodo actual por un cero
	left.gws[left.current_pos] = false;
	left.current_m--;
	// Si es una hoja, apuntarlo y calcular el coste como el de una hoja
	if (is_leaf(p, left)) {
		left.is_leaf = true;
		std::fill(left.gws.begin() + left.current_pos,
				  left.gws.end(),
				  false);
		left.cost = p.calc_cost(left.gws);
	} else {
		// No es una hoja, calcular el coste con la cota optimista
		left.is_leaf = false;
		left.cost = p.calc_cost(left.gws);
	}
	left.current_pos++;

	// El hijo de la derecha no cambia ninguna puerta de enlace, por lo que solo cambia su posicion
	// Si es una hoja, apuntarlo y calcular el coste como si fuera una hoja
	right.current_pos++;
	if (is_leaf(p, right)) {
		right.is_leaf = true;
		std::fill(right.gws.begin() + right.current_pos,
				  right.gws.end(),
				  false);
		right.cost = p.calc_cost(right.gws);
	} else {
		right.is_leaf = false;
		// El coste se mantiene igual
	}

	return new_nodes;
}

bool is_feasible(const Problem& p, const node& n)
{
	// Si el nodo esta fuera de rango, eliminar
	if (n.current_pos > p.get_n())
		return false;

	// Si el nodo contiene menos puertas de enlace que las que requiere la solucion, eliminar
	if (n.current_m < p.get_m())
		return false;

	return true;
}

void met_bb(const Problem& p,
			NodeCount& nc,
			float& best_v)
{
	// Cola de prioridad para guardar los nodos vivos
	priority_queue pq(compare_func);

	// Insertar el nodo inicial en la cola
	node initial;
	initial.current_pos = 0,
	initial.current_m = p.get_n(),
	initial.cost = 0.0f,
	initial.gws = gateways(p.get_n(), true),
	initial.is_leaf = false,
	pq.push(initial);

	// Hasta que no queden nodos vivos, seguir explorando
	while(not pq.empty()) {
		node n = pq.top();
		pq.pop();

		// Comparar con la cota optimista actual
		if (n.cost >= best_v) {
			nc.nodos_prometedores_descartados++;
			continue;
		}

		// Por cada nodo sacado, siempre se exploran dos nodos mas
		nc.nodos_explorados += 2;
		nc.nodos_expandidos++;

		for (const auto new_node : expand(p, n)) {
			// Si el nodo no es factible, no se tiene en cuenta
			if (not is_feasible(p, new_node)) {
				nc.nodos_no_factibles++;
				continue;
			}

			// Si el nodo es una hoja, comparar con la mejor solucion actual
			if (new_node.is_leaf) {
				// Si es mejor que la cota pesimista actual, actualizar
				if (new_node.cost < best_v) {
					nc.veces_solucion_actualizada++;
					best_v = new_node.cost;
				}
				continue;
			}

			// Si el nodo tiene un coste menor que la cota pesimista, guardarlo en la cola de nodos vivos
			if (new_node.cost < best_v) {
				pq.push(new_node);

				// Se cuenta el nodo aniadido
				nc.nodos_anadidos++;
			} else {
				// Nodo descartado por no ser prometedor
				nc.nodos_no_prometedores++;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	if (argc != 3) {
		std::cerr << "ERROR: Numero de parametros incorrectos\n";
		exit(-1);
	}

	// Leer argumentos
	int argcnt = 1;
	std::string file_path;
	while (argcnt < argc) {
		if (std::string(argv[argcnt]).compare("-f") == 0) {
			if (argcnt + 1 < argc) {
				file_path = argv[argcnt+1];
				argcnt += 2;
			}
			else {
				std::cout << "Error: file not specified" << std::endl;
				exit(-1);
			}
		}
	}

	if (file_path.empty()) {
		std::cout << "Error: file not specified" << std::endl;
		exit(-1);
	}

	Problem problem(file_path);
	std::vector<size_t> indices1, indices2, indices;
	std::vector<size_t> order;

	auto start = std::chrono::high_resolution_clock::now();

	// Se calculan los dos metodos de voraz y nos quedamos con el mejor
	float greedy_1 = greedy_criteria_1(problem, indices1);
	float greedy_2 = greedy_criteria_2(problem, indices2);
	float greedy;
	if (greedy_1 < greedy_2) {
		greedy = greedy_1;
		indices = indices1;
	} else {
		greedy = greedy_2;
		indices = indices2;
	}

	//order = problem.sort_by_cost();
	
	float result = greedy;
	NodeCount nc = {0};
	met_bb(problem, nc, result);

	auto end = std::chrono::high_resolution_clock::now();

	// Imprime el resultado
	std::cout << result << std::endl;
	// Imprime el recuento de los nodos
	std::cout
		<< nc.nodos_explorados << " "
		<< nc.nodos_anadidos << " "
		<< nc.nodos_no_prometedores << " "
		<< nc.nodos_no_factibles << " "
		<< nc.nodos_completados << " "
		<< nc.nodos_prometedores_descartados << " "
		<< nc.nodos_expandidos << " "
		<< nc.veces_solucion_actualizada << " "
		<< nc.veces_solucion_actulizada_cota_pesimista << std::endl;
	// Imprime el tiempo total de ejecucion del algoritmo
	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()/1000.0 <<  std::endl;

	return 0;
}	
